# Change Log

## v0.2.1
- Add missing lifetime to slice returned by `ResolvedRecord::data_value() -> Option<&'s [u8]>`
