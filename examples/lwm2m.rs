// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use std::str::FromStr;

use senml_deser::{
    content::{
        pack::Pack,
        record::{Record, Values},
    },
    deser::{de::Decoder, ser::Encoder},
    Cbor,
};

// Test against the example given by the Lightweight M2M Core SPecifications 1.1.1
// https://www.openmobilealliance.org/release/LightweightM2M/V1_1_1-20190617-A/OMA-TS-LightweightM2M_Core-V1_1_1-20190617-A.pdf
// Section 7.4.6, Page 86/87

#[derive(Debug)]
struct Lwm2mPath {
    _oid: u16,
    _oiid: Option<u16>,
    rid: Option<u16>,
    riid: Option<u16>,
}

impl Lwm2mPath {
    fn new(path: &str) -> Self {
        let parts: Vec<&str> = path.split("/").collect();

        Self {
            _oid: parts.get(1).unwrap().parse::<u16>().unwrap(),
            _oiid: parts.get(2).map(|s| s.parse::<u16>().unwrap()),
            rid: parts.get(3).map(|s| s.parse::<u16>().unwrap()),
            riid: parts.get(4).map(|s| s.parse::<u16>().unwrap()),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Resource<T> {
    id: u16,
    value: T,
}

#[derive(Debug, PartialEq, Eq)]
struct ResourceMulti<T, const N: usize> {
    id: u16,
    value: [T; N],
}

// This struct does not meet the specification
// It is just fit to the example
#[derive(Debug, PartialEq, Eq)]
struct Device {
    id: u16,
    manufacturer: Resource<heapless::String<32>>,
    model_number: Resource<heapless::String<32>>,
    serial_number: Resource<heapless::String<32>>,
    firmware_version: Resource<heapless::String<32>>,
    available_power_sources: ResourceMulti<u8, 2>,
    power_source_voltage: ResourceMulti<i32, 2>,
    power_source_current: ResourceMulti<i32, 2>,
    battery_level: Resource<u8>,
    memory_free: Resource<u32>,
    error_code: ResourceMulti<u8, 1>,
    current_time: Resource<u64>,
    utc_offset: Resource<heapless::String<32>>,
    supported_binding_modes: Resource<heapless::String<32>>,
}

impl Device {
    fn example() -> Self {
        Self {
            id: 3,
            manufacturer: Resource {
                id: 0,
                value: heapless::String::from_str("Open Mobile Alliance").unwrap(),
            },
            model_number: Resource {
                id: 1,
                value: heapless::String::from_str("Lightweight M2M Client").unwrap(),
            },
            serial_number: Resource {
                id: 2,
                value: heapless::String::from_str("345000123").unwrap(),
            },
            firmware_version: Resource {
                id: 3,
                value: heapless::String::from_str("1.0").unwrap(),
            },
            available_power_sources: ResourceMulti {
                id: 6,
                value: [1, 5],
            },
            power_source_voltage: ResourceMulti {
                id: 7,
                value: [3800, 5000],
            },
            power_source_current: ResourceMulti {
                id: 8,
                value: [125, 900],
            },
            battery_level: Resource { id: 9, value: 100 },
            memory_free: Resource { id: 10, value: 15 },
            error_code: ResourceMulti { id: 11, value: [0] },
            current_time: Resource {
                id: 13,
                value: 1367491215_u64,
            },
            utc_offset: Resource {
                id: 14,
                value: heapless::String::from_str("+02:00").unwrap(),
            },
            supported_binding_modes: Resource {
                id: 16,
                value: heapless::String::from_str("U").unwrap(),
            },
        }
    }

    fn zero() -> Self {
        Self {
            id: 3,
            manufacturer: Resource {
                id: 0,
                value: heapless::String::from_str("").unwrap(),
            },
            model_number: Resource {
                id: 1,
                value: heapless::String::from_str("").unwrap(),
            },
            serial_number: Resource {
                id: 2,
                value: heapless::String::from_str("").unwrap(),
            },
            firmware_version: Resource {
                id: 3,
                value: heapless::String::from_str("").unwrap(),
            },
            available_power_sources: ResourceMulti {
                id: 6,
                value: [0, 0],
            },
            power_source_voltage: ResourceMulti {
                id: 7,
                value: [0, 0],
            },
            power_source_current: ResourceMulti {
                id: 8,
                value: [0, 0],
            },
            battery_level: Resource { id: 9, value: 0 },
            memory_free: Resource { id: 10, value: 0 },
            error_code: ResourceMulti { id: 11, value: [0] },
            current_time: Resource {
                id: 13,
                value: 0_u64,
            },
            utc_offset: Resource {
                id: 14,
                value: heapless::String::from_str("").unwrap(),
            },
            supported_binding_modes: Resource {
                id: 16,
                value: heapless::String::from_str("").unwrap(),
            },
        }
    }
}

fn main() {
    // SenML CBOR representation of the following SenML JSON
    // [
    // {-2: "/3/0/", 0: "0", 3: "Open Mobile Alliance"},
    // {0: "1", 3: "Lightweight M2M Client"},
    // {0: "2", 3: "345000123"},
    // {0: "3", 3: "1.0"},
    // {0: "6/0", 2: 1},
    // {0: "6/1", 2: 5},
    // {0: "7/0", 2: 3800},
    // {0: "7/1", 2: 5000},
    // {0: "8/0", 2: 125},
    // {0: "8/1", 2: 900},
    // {0: "9", 2: 100},
    // {0: "10", 2: 15},
    // {0: "11/0", 2: 0},
    // {0: "13", 2: 1367491215},
    // {0: "14", 3: "+02:00"},
    // {0: "16", 3: "U"}
    // ]
    let expected_senml_cbor = [
        0x90, 0xa3, 0x21, 0x65, 0x2f, 0x33, 0x2f, 0x30, 0x2f, 0x00, 0x61, 0x30, 0x03, 0x74, 0x4f,
        0x70, 0x65, 0x6e, 0x20, 0x4d, 0x6f, 0x62, 0x69, 0x6c, 0x65, 0x20, 0x41, 0x6c, 0x6c, 0x69,
        0x61, 0x6e, 0x63, 0x65, 0xa2, 0x00, 0x61, 0x31, 0x03, 0x76, 0x4c, 0x69, 0x67, 0x68, 0x74,
        0x77, 0x65, 0x69, 0x67, 0x68, 0x74, 0x20, 0x4d, 0x32, 0x4d, 0x20, 0x43, 0x6c, 0x69, 0x65,
        0x6e, 0x74, 0xa2, 0x00, 0x61, 0x32, 0x03, 0x69, 0x33, 0x34, 0x35, 0x30, 0x30, 0x30, 0x31,
        0x32, 0x33, 0xa2, 0x00, 0x61, 0x33, 0x03, 0x63, 0x31, 0x2e, 0x30, 0xa2, 0x00, 0x63, 0x36,
        0x2f, 0x30, 0x02, 0x01, 0xa2, 0x00, 0x63, 0x36, 0x2f, 0x31, 0x02, 0x05, 0xa2, 0x00, 0x63,
        0x37, 0x2f, 0x30, 0x02, 0x19, 0x0e, 0xd8, 0xa2, 0x00, 0x63, 0x37, 0x2f, 0x31, 0x02, 0x19,
        0x13, 0x88, 0xa2, 0x00, 0x63, 0x38, 0x2f, 0x30, 0x02, 0x18, 0x7d, 0xa2, 0x00, 0x63, 0x38,
        0x2f, 0x31, 0x02, 0x19, 0x03, 0x84, 0xa2, 0x00, 0x61, 0x39, 0x02, 0x18, 0x64, 0xa2, 0x00,
        0x62, 0x31, 0x30, 0x02, 0x0f, 0xa2, 0x00, 0x64, 0x31, 0x31, 0x2f, 0x30, 0x02, 0x00, 0xa2,
        0x00, 0x62, 0x31, 0x33, 0x02, 0x1a, 0x51, 0x82, 0x42, 0x8f, 0xa2, 0x00, 0x62, 0x31, 0x34,
        0x03, 0x66, 0x2b, 0x30, 0x32, 0x3a, 0x30, 0x30, 0xa2, 0x00, 0x62, 0x31, 0x36, 0x03, 0x61,
        0x55,
    ];

    let device = Device::example();

    println!("Attempting to encode the Device Object: {device:?}");
    let mut pack = Pack::<Cbor>::new();

    let record_1 = Record::<Cbor> {
        base_name: Some("/3/0/"),
        name: Some("0"),
        value: Some(Values::StringValue(device.manufacturer.value.as_str())),
        ..Default::default()
    };

    pack.add_record(record_1).unwrap();

    pack.add_string_value_record("1", device.model_number.value.as_str())
        .unwrap();

    pack.add_string_value_record("2", "345000123").unwrap();

    pack.add_string_value_record("3", "1.0").unwrap();

    pack.add_value_record("6/0", 1_f64).unwrap();

    pack.add_value_record("6/1", 5_f64).unwrap();

    pack.add_value_record("7/0", 3800_f64).unwrap();

    pack.add_value_record("7/1", 5000_f64).unwrap();

    pack.add_value_record("8/0", 125_f64).unwrap();

    pack.add_value_record("8/1", 900_f64).unwrap();

    pack.add_value_record("9", 100_f64).unwrap();

    pack.add_value_record("10", 15_f64).unwrap();

    pack.add_value_record("11/0", 0_f64).unwrap();

    pack.add_value_record("13", 1367491215_f64).unwrap();

    pack.add_string_value_record("14", "+02:00").unwrap();

    pack.add_string_value_record("16", "U").unwrap();

    let mut buf = [0_u8; 1024];
    let mut encoder = Encoder::new(&mut buf);

    if let Ok(encoded_data) = encoder.cbor_encode(pack) {
        println!(
            "encoded:\n{:02x?}\nlength: {}\n",
            encoded_data,
            encoded_data.len()
        );
        println!(
            "expected:\n{:02x?}\nlength: {}\n",
            expected_senml_cbor,
            expected_senml_cbor.len()
        );
        // Compare to expected_senml_cbor
        assert_eq!(encoded_data, expected_senml_cbor);
        println!("Expected and Encoded are identical!");

        let mut decoder = Decoder::new();
        let mut scratch = [0u8; 2000];
        let decoded_data;
        let result = decoder.decode_cbor(encoded_data, &mut scratch);
        if let Err(error) = result {
            match error {
                senml_deser::Error::Deserialize(e) => {
                    println!("{}", e.to_string())
                }
                _ => return,
            };

            return;
        } else {
            decoded_data = result.unwrap();
        }

        let mut decoded_device = Device::zero();
        // assert_eq!(expected_senml_cbor, encoded_data);

        for record in decoded_data.records_iter() {
            if let Ok(record) = record {
                let path = Lwm2mPath::new(record.resolved_name::<34>().unwrap().as_str());

                // In practice you would probably have some kind of object/resource manager
                // that distributed the data to the correct resource based on the path
                if let Some(resource_id) = path.rid {
                    match resource_id {
                        0 => {
                            decoded_device.manufacturer.value =
                                heapless::String::<32>::from_str(record.string_value().unwrap())
                                    .unwrap()
                        }
                        1 => {
                            decoded_device.model_number.value =
                                heapless::String::<32>::from_str(record.string_value().unwrap())
                                    .unwrap()
                        }
                        2 => {
                            decoded_device.serial_number.value =
                                heapless::String::<32>::from_str(record.string_value().unwrap())
                                    .unwrap()
                        }
                        3 => {
                            decoded_device.firmware_version.value =
                                heapless::String::<32>::from_str(record.string_value().unwrap())
                                    .unwrap()
                        }
                        6 => {
                            let instance_id = path.riid.unwrap() as usize;
                            decoded_device.available_power_sources.value[instance_id] =
                                record.resolved_value().unwrap() as u8;
                        }
                        7 => {
                            let instance_id = path.riid.unwrap() as usize;
                            decoded_device.power_source_voltage.value[instance_id] =
                                record.resolved_value().unwrap() as i32;
                        }
                        8 => {
                            let instance_id = path.riid.unwrap() as usize;
                            decoded_device.power_source_current.value[instance_id] =
                                record.resolved_value().unwrap() as i32;
                        }
                        9 => {
                            decoded_device.battery_level.value =
                                record.resolved_value().unwrap() as u8
                        }
                        10 => {
                            decoded_device.memory_free.value =
                                record.resolved_value().unwrap() as u32
                        }
                        11 => {
                            let instance_id = path.riid.unwrap() as usize;
                            decoded_device.error_code.value[instance_id] =
                                record.resolved_value().unwrap() as u8;
                        }
                        13 => {
                            decoded_device.current_time.value =
                                record.resolved_value().unwrap() as u64
                        }
                        14 => {
                            decoded_device.utc_offset.value =
                                heapless::String::<32>::from_str(record.string_value().unwrap())
                                    .unwrap()
                        }
                        16 => {
                            decoded_device.supported_binding_modes.value =
                                heapless::String::<32>::from_str(record.string_value().unwrap())
                                    .unwrap()
                        }
                        _ => println!("Unknown Resource Id: {}", resource_id),
                    }
                }
            }
        }

        println!("Decoded Device: {decoded_device:?}");

        assert_eq!(device, decoded_device);
        println!("Device successfully encoded and decoded again.");
    }
}
