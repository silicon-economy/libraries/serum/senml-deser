// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use senml_deser::{
    content::{
        pack::Pack,
        record::{Record, Values},
    },
    deser::ser::Encoder,
    Cbor,
};

fn main() {
    // creating the package
    let mut pack = Pack::new();

    // Create a common Record that can hold any SenML fields
    let record = Record::<Cbor> {
        base_name: Some("urn:dev:ow:10e2073a0108006:"),
        base_time: Some(1276020076.001),
        base_version: Some(5),
        base_unit: Some("A"),
        name: Some("voltage"),
        unit: Some("V"),
        value: Some(Values::Value(120.1)),
        ..Default::default()
    };
    pack.add_record(record).unwrap();

    // Use convenience function to directly add common name/value records
    pack.add_time_value_record("current", -5_f64, 1.2).unwrap();

    pack.add_time_value_record("current", -4_f64, 1.3).unwrap();

    pack.add_time_value_record("current", -3_f64, 1.4).unwrap();

    pack.add_time_value_record("current", -2_f64, 1.5).unwrap();

    pack.add_time_value_record("current", -1_f64, 1.6).unwrap();

    pack.add_time_value_record("current", 0_f64, 1.7).unwrap();

    // Crate the encoder and encode the package
    let mut buf = [0_u8; 1024];
    let mut encoder = Encoder::new(&mut buf);

    // Get the encoded bytes
    if let Ok(encoded_data) = encoder.cbor_encode(pack) {
        println!(
            "encoded:\n{:02x?}\nlength: {}\n",
            encoded_data,
            encoded_data.len()
        );
    }
}
