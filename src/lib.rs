// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

#![warn(missing_docs)]
#![doc = include_str!("../README.md")]
#![no_std]

pub mod content;
pub mod deser;

const DEFAULT_MAX_PACKAGE_LENGTH: usize = 32;

/// Specifications for configuring de-/serialization
#[derive(Debug, Clone)]
pub struct Cbor;

/// Error for SenML encoding and decoding
#[derive(Debug)]
pub enum Error {
    /// Something went wrong while adding a key to the map
    InsertKey,
    /// Entry already contains a name
    DoubleName,
    /// Entry already contains the field
    DoubleField,
    /// Unknown field
    UndefinedField(i8),
    /// Some Error in the Format
    UndefinedFormat,
    /// No name was Defined in the entry and no base name was set
    MissingName,
    /// Type of value isn't allowed for that label
    MissMatchedLabelValue,
    /// Exceeded max amount of Entries
    TooManyEntries,
    /// No Entries were added
    TooFewEntries,
    /// Some error during serialization occurred
    Serialize(serde_cbor::Error),
    /// Some error during deserialization occurred
    Deserialize(serde_cbor::Error),
    /// String was too long
    StringTooLong,
    /// Value exceeded max value for given type
    MaxValueExceeded,
    /// Value is too low for given type
    ValueTooLow,
    /// At least one value field must be set in a valid entry
    MissingValueField,
}
