// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Deserializer

use crate::{content::pack::Pack, Cbor};

/// Decoder that internally uses serde to deserialize data
pub struct Decoder;

impl<'de> Decoder {
    /// Creates a new Decoder
    pub fn new() -> Self {
        Decoder {}
    }

    /// Decodes a given message that is encoded in CBOR
    ///
    /// The user needs to supply a buffer called scratch that is used during decoding.
    pub fn decode_cbor(
        &'de mut self,
        value: &'de [u8],
        scratch: &mut [u8],
    ) -> Result<Pack<Cbor>, crate::Error> {
        let cbor_package: Pack<Cbor> = serde_cbor::de::from_slice_with_scratch(value, scratch)
            .map_err(crate::Error::Deserialize)?;
        Ok(cbor_package)
    }
}

impl Default for Decoder {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::content::record::{Record, Values};
    use crate::Cbor;

    #[test]
    fn test_decode_cbor() {
        let mut reference_pack = Pack::<Cbor>::new();
        let record = Record::<Cbor> {
            base_name: Some("urn:dev:ow:10e2073a0108006:"),
            base_time: Some(1276020076.001),
            base_version: Some(5),
            base_unit: Some("A"),
            name: Some("voltage"),
            unit: Some("V"),
            value: Some(Values::Value(120.1)),
            ..Record::<Cbor>::new()
        };

        reference_pack.add_record(record).unwrap();
        let record = Record::<Cbor> {
            name: Some("current"),
            time: Some(-5_f64),
            value: Some(Values::Value(1.2)),
            ..Record::<Cbor>::new()
        };

        reference_pack.add_record(record).unwrap();
        let record = Record::<Cbor> {
            name: Some("current"),
            time: Some(-4_f64),
            value: Some(Values::Value(1.3)),
            ..Record::<Cbor>::new()
        };

        reference_pack.add_record(record).unwrap();
        let record = Record::<Cbor> {
            name: Some("current"),
            time: Some(-3_f64),
            value: Some(Values::Value(1.4)),
            ..Record::<Cbor>::new()
        };

        reference_pack.add_record(record).unwrap();
        let record = Record::<Cbor> {
            name: Some("current"),
            time: Some(-2_f64),
            value: Some(Values::Value(1.5)),
            ..Record::<Cbor>::new()
        };

        reference_pack.add_record(record).unwrap();
        let record = Record::<Cbor> {
            name: Some("current"),
            time: Some(-1_f64),
            value: Some(Values::Value(1.6)),
            ..Record::<Cbor>::new()
        };

        reference_pack.add_record(record).unwrap();
        let record = Record::<Cbor> {
            name: Some("current"),
            time: Some(0_f64),
            value: Some(Values::Value(1.7)),
            ..Record::<Cbor>::new()
        };

        reference_pack.add_record(record).unwrap();

        // Have the encoded data
        let encoded_data: &[u8] = &[
            0x87, 0xa7, 0x21, 0x78, 0x1b, 0x75, 0x72, 0x6e, 0x3a, 0x64, 0x65, 0x76, 0x3a, 0x6f,
            0x77, 0x3a, 0x31, 0x30, 0x65, 0x32, 0x30, 0x37, 0x33, 0x61, 0x30, 0x31, 0x30, 0x38,
            0x30, 0x30, 0x36, 0x3a, 0x22, 0xfb, 0x41, 0xd3, 0x03, 0xa1, 0x5b, 0x00, 0x10, 0x62,
            0x23, 0x61, 0x41, 0x20, 0x05, 0x00, 0x67, 0x76, 0x6f, 0x6c, 0x74, 0x61, 0x67, 0x65,
            0x01, 0x61, 0x56, 0x02, 0xfb, 0x40, 0x5e, 0x06, 0x66, 0x66, 0x66, 0x66, 0x66, 0xa3,
            0x00, 0x67, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x06, 0x24, 0x02, 0xfb, 0x3f,
            0xf3, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0xa3, 0x00, 0x67, 0x63, 0x75, 0x72, 0x72,
            0x65, 0x6e, 0x74, 0x06, 0x23, 0x02, 0xfb, 0x3f, 0xf4, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc,
            0xcd, 0xa3, 0x00, 0x67, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x06, 0x22, 0x02,
            0xfb, 0x3f, 0xf6, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0xa3, 0x00, 0x67, 0x63, 0x75,
            0x72, 0x72, 0x65, 0x6e, 0x74, 0x06, 0x21, 0x02, 0xf9, 0x3e, 0x00, 0xa3, 0x00, 0x67,
            0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x06, 0x20, 0x02, 0xfb, 0x3f, 0xf9, 0x99,
            0x99, 0x99, 0x99, 0x99, 0x9a, 0xa3, 0x00, 0x67, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e,
            0x74, 0x06, 0x00, 0x02, 0xfb, 0x3f, 0xfb, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33,
        ];
        // Create the Decoder
        let mut decoder = Decoder::new();
        // We need to provide a scratch array for the decoder
        let mut scratch = [0_u8; 1024];
        // Deserialize the data
        let decoded_package = decoder.decode_cbor(encoded_data, &mut scratch).unwrap();
        // The Package supplies a EntryIterator to iterate over the decoded entries
        for (decoded_entry, reference_entry) in decoded_package
            .records_iter()
            .zip(reference_pack.records_iter())
        {
            if let Ok(decoded_entry) = decoded_entry {
                if let Ok(reference_entry) = reference_entry {
                    assert_eq!(decoded_entry, reference_entry)
                }
            }
        }
    }
}
