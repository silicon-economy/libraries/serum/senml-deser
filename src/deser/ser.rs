// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Serializer

use crate::content::pack::Pack;
use crate::Cbor;
use serde::Serialize;
use serde_cbor::ser::SliceWrite;
use serde_cbor::Serializer as serde_cbor_serializer;

/// Encoder that internally uses serde to serialize data
///
/// Currently no streaming encoding supported
pub struct Encoder<'out> {
    /// Indicates, whether we stream data(not implemented)
    _streaming: bool, // TODO Streaming of Messages
    /// contains encoded data
    out_buf: &'out mut [u8],
}

impl<'out> Encoder<'out> {
    /// Create a new Encoder
    pub fn new(out_buf: &'out mut [u8]) -> Self {
        Encoder {
            _streaming: false,
            out_buf,
        }
    }

    /// Encodes a [Pack] using CBOR specs
    pub fn cbor_encode(&'out mut self, data: Pack<Cbor>) -> Result<&'out [u8], crate::Error> {
        self.out_buf.fill(0_u8); // resetting buffer to 0

        let writer = SliceWrite::new(self.out_buf);
        let mut serializer = serde_cbor_serializer::new(writer);

        data.serialize(&mut serializer)
            .map_err(crate::Error::Serialize)?;

        let writer = serializer.into_inner();
        let bytes_written = writer.bytes_written();
        Ok(&self.out_buf[..bytes_written])
    }
}
