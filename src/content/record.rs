// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! The [Record] represents one SenML record.
//!
//! The [Record] is part of the public Interface, that is actually encodable and decodable.
//! It holds the information of all available SenML fields
//!
//! The specification S selects the format of serialization. Currently supported are:\
//!     1.Cbor
use serde::{de::Visitor, ser::SerializeMap, Deserialize, Serialize, Serializer};

use crate::{content::label::Label, Cbor};

/// Contains the different Value Types of a record
#[derive(Clone, Copy, Debug)]
pub enum Values<'s> {
    /// Contains an f64
    Value(f64),
    /// Contains an str
    StringValue(&'s str),
    /// Contains a bool
    BooleanValue(bool),
    /// Contains a slice of u8
    DataValue(&'s [u8]),
}

impl<'s> PartialEq<Values<'s>> for Values<'s> {
    fn eq(&self, other: &Values<'s>) -> bool {
        match (self, other) {
            (Self::Value(l0), Self::Value(r0)) => l0 == r0,
            (Self::StringValue(l0), Self::StringValue(r0)) => l0 == r0,
            (Self::BooleanValue(l0), Self::BooleanValue(r0)) => l0 == r0,
            (Self::DataValue(l0), Self::DataValue(r0)) => l0 == r0,
            _ => false,
        }
    }
}

impl<'s> Serialize for Values<'s> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        use self::Values::*;
        match *self {
            Value(v) => {
                if v == (v as i64) as f64 {
                    serializer.serialize_i64(v as i64)
                } else {
                    serializer.serialize_f64(v)
                }
            }
            StringValue(s) => serializer.serialize_str(s),
            BooleanValue(b) => serializer.serialize_bool(b),
            DataValue(d) => serializer.serialize_bytes(d),
        }
    }
}

impl<'de> Deserialize<'de> for Values<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct ValueVisitor;

        impl<'de> Visitor<'de> for ValueVisitor {
            type Value = Values<'de>;

            fn expecting(&self, formatter: &mut core::fmt::Formatter) -> core::fmt::Result {
                formatter.write_str("expecting Value enum")
            }

            fn visit_u8<E>(self, v: u8) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_u16<E>(self, v: u16) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_u32<E>(self, v: u32) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_i8<E>(self, v: i8) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_i16<E>(self, v: i16) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_i32<E>(self, v: i32) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v as f64))
            }

            fn visit_f64<E>(self, v: f64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::Value(v))
            }

            fn visit_borrowed_str<E>(self, v: &'de str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::StringValue(v))
            }

            fn visit_bool<E>(self, v: bool) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::BooleanValue(v))
            }

            fn visit_borrowed_bytes<E>(self, v: &'de [u8]) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Values::DataValue(v))
            }
        }

        deserializer.deserialize_any(ValueVisitor)
    }
}

/// Holds the information of one SenML record
#[derive(Debug, Clone)]
pub struct Record<'s, S: Clone> {
    /// Base version field
    pub base_version: Option<u64>,
    /// Base name field
    pub base_name: Option<&'s str>,
    /// Base time field
    pub base_time: Option<f64>,
    /// Base unit field
    pub base_unit: Option<&'s str>,
    /// Base value field
    pub base_value: Option<f64>,
    /// Base sum field
    pub base_sum: Option<f64>,
    /// Name field
    pub name: Option<&'s str>,
    /// Unit field
    pub unit: Option<&'s str>,
    /// Holds the value of the [Record], can be one of:\
    /// 1. value(f64)
    /// 2. StringValue(&'s str)
    /// 3. BooleanValue(bool)
    /// 4. DataValue(&'s [u8])
    pub value: Option<Values<'s>>,
    /// Sum field
    pub sum: Option<f64>,
    /// Time field
    pub time: Option<f64>,
    /// Update time field
    pub update_time: Option<f64>,

    /// phantom data
    pub _marker: core::marker::PhantomData<S>,
}

impl<'s, S: Clone> Record<'s, S> {
    /// Creates a new [Record]
    pub fn new() -> Record<'s, S> {
        Record {
            base_version: None,
            base_name: None,
            base_time: None,
            base_unit: None,
            base_value: None,
            base_sum: None,
            name: None,
            unit: None,
            value: None,
            sum: None,
            time: None,
            update_time: None,
            _marker: core::marker::PhantomData::default(),
        }
    }

    /// counts the number of fields that are set and have to be serialized
    fn count_fields(&self) -> usize {
        let mut counter = 0;
        if self.base_version.is_some() {
            counter += 1;
        }
        if self.base_name.is_some() {
            counter += 1;
        }
        if self.base_time.is_some() {
            counter += 1;
        }
        if self.base_unit.is_some() {
            counter += 1;
        }
        if self.base_value.is_some() {
            counter += 1;
        }
        if self.base_sum.is_some() {
            counter += 1;
        }
        if self.name.is_some() {
            counter += 1;
        }
        if self.unit.is_some() {
            counter += 1;
        }
        if self.value.is_some() {
            counter += 1;
        }
        if self.sum.is_some() {
            counter += 1;
        }
        if self.time.is_some() {
            counter += 1;
        }
        if self.update_time.is_some() {
            counter += 1;
        }
        counter
    }
}

impl<'s, S: Clone> Default for Record<'s, S> {
    fn default() -> Self {
        Record {
            base_version: None,
            base_name: None,
            base_time: None,
            base_unit: None,
            base_value: None,
            base_sum: None,
            name: None,
            unit: None,
            value: None,
            sum: None,
            time: None,
            update_time: None,
            _marker: core::marker::PhantomData::default(),
        }
    }
}

impl<'s> Serialize for Record<'s, Cbor> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        use crate::content::label::Label::*;
        let mut map = serializer.serialize_map(Some(self.count_fields()))?;
        if let Some(base_version) = self.base_version {
            map.serialize_key(&BaseVersion.cbor_value())?;
            map.serialize_value(&base_version)?;
        }

        if let Some(base_name) = self.base_name {
            map.serialize_key(&BaseName.cbor_value())?;
            map.serialize_value(&base_name)?;
        }

        if let Some(base_time) = self.base_time {
            map.serialize_key(&BaseTime.cbor_value())?;

            if base_time == (base_time as i64) as f64 {
                map.serialize_value(&(base_time as i64))?;
            } else {
                map.serialize_value(&base_time)?;
            }
        }

        if let Some(base_unit) = self.base_unit {
            map.serialize_key(&BaseUnit.cbor_value())?;
            map.serialize_value(&base_unit)?;
        }

        if let Some(base_value) = self.base_value {
            map.serialize_key(&BaseValue.cbor_value())?;
            map.serialize_value(&base_value)?;
        }

        if let Some(base_sum) = self.base_sum {
            map.serialize_key(&BaseSum.cbor_value())?;
            map.serialize_value(&base_sum)?;
        }

        if let Some(name) = self.name {
            map.serialize_key(&Name.cbor_value())?;
            map.serialize_value(&name)?;
        }

        if let Some(unit) = self.unit {
            map.serialize_key(&Unit.cbor_value())?;
            map.serialize_value(&unit)?;
        }

        if let Some(value) = self.value {
            match value {
                crate::content::record::Values::Value(_) => {
                    map.serialize_key(&Value.cbor_value())?
                }
                crate::content::record::Values::StringValue(_) => {
                    map.serialize_key(&StringValue.cbor_value())?
                }
                crate::content::record::Values::BooleanValue(_) => {
                    map.serialize_key(&BooleanValue.cbor_value())?
                }
                crate::content::record::Values::DataValue(_) => {
                    map.serialize_key(&DataValue.cbor_value())?
                }
            };
            map.serialize_value(&value)?;
        }

        if let Some(sum) = self.sum {
            map.serialize_key(&Sum.cbor_value())?;
            map.serialize_value(&sum)?;
        }

        if let Some(time) = self.time {
            map.serialize_key(&Time.cbor_value())?;
            if time == (time as i64) as f64 {
                map.serialize_value(&(time as i64))?;
            } else {
                map.serialize_value(&time)?;
            }
        }

        if let Some(update_time) = self.update_time {
            map.serialize_key(&UpdateTime.cbor_value())?;
            if update_time == (update_time as i64) as f64 {
                map.serialize_value(&(update_time as i64))?;
            } else {
                map.serialize_value(&update_time)?;
            }
        }

        map.end()
    }
}

impl<'de> Deserialize<'de> for Record<'de, Cbor> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct EntriesVisitor;

        impl<'de> Visitor<'de> for EntriesVisitor {
            type Value = Record<'de, Cbor>;

            fn expecting(&self, formatter: &mut core::fmt::Formatter) -> core::fmt::Result {
                write!(formatter, "expecting a sequence of maps: (key: value)")
            }
            fn visit_map<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: serde::de::MapAccess<'de>,
            {
                let mut record = Record::<Cbor>::new();
                while let Some(key) = seq.next_key::<i8>()? {
                    let label = Label::try_from(key).unwrap();
                    match label {
                        Label::BaseVersion => record.base_version = Some(seq.next_value()?),
                        Label::BaseName => record.base_name = Some(seq.next_value()?),
                        Label::BaseTime => record.base_time = Some(seq.next_value()?),
                        Label::BaseUnit => record.base_unit = Some(seq.next_value()?),
                        Label::BaseValue => record.base_value = Some(seq.next_value()?),
                        Label::BaseSum => record.base_sum = Some(seq.next_value()?),
                        Label::Name => record.name = Some(seq.next_value()?),
                        Label::Unit => record.unit = Some(seq.next_value()?),
                        Label::Value => record.value = Some(seq.next_value()?),
                        Label::StringValue => record.value = Some(seq.next_value()?),
                        Label::BooleanValue => record.value = Some(seq.next_value()?),
                        Label::Sum => record.sum = Some(seq.next_value()?),
                        Label::Time => record.time = Some(seq.next_value()?),
                        Label::UpdateTime => record.update_time = Some(seq.next_value()?),
                        Label::DataValue => record.value = Some(seq.next_value()?),
                    };
                }
                Ok(record)
            }
        }
        deserializer.deserialize_seq(EntriesVisitor)
    }
}

impl<'s, S: Clone> PartialEq for Record<'s, S> {
    fn eq(&self, other: &Self) -> bool {
        self.base_version == other.base_version
            && self.base_name == other.base_name
            && self.base_time == other.base_time
            && self.base_unit == other.base_unit
            && self.base_value == other.base_value
            && self.base_sum == other.base_sum
            && self.name == other.name
            && self.unit == other.unit
            && self.value == other.value
            && self.sum == other.sum
            && self.time == other.time
            && self.update_time == other.update_time
            && self._marker == other._marker
    }
}

#[cfg(test)]
mod test {

    extern crate std;

    use super::*;
    use rand::prelude::*;
    use rand_chacha::ChaCha20Rng;
    use serde::Serialize;
    use serde_cbor::ser::SliceWrite;
    use serde_cbor::Serializer as serde_cbor_serializer;

    /// Serializing [Record]'s
    #[test]
    fn test_serialize_record() {
        let reference_bytes = [
            167, 32, 5, 33, 120, 27, 117, 114, 110, 58, 100, 101, 118, 58, 111, 119, 58, 49, 48,
            101, 50, 48, 55, 51, 97, 48, 49, 48, 56, 48, 48, 54, 58, 34, 251, 65, 211, 3, 161, 91,
            0, 16, 98, 35, 97, 65, 0, 103, 118, 111, 108, 116, 97, 103, 101, 1, 97, 86, 2, 251, 64,
            94, 6, 102, 102, 102, 102, 102,
        ];

        let mut buf = [0_u8; 1024];
        let writer = SliceWrite::new(&mut buf);
        let mut serializer = serde_cbor_serializer::new(writer);
        let record = Record::<Cbor> {
            base_version: Some(5),
            base_name: Some("urn:dev:ow:10e2073a0108006:"),
            base_time: Some(1276020076.001),
            base_unit: Some("A"),
            name: Some("voltage"),
            unit: Some("V"),
            value: Some(Values::Value(120.1)),
            ..Default::default()
        };
        record.serialize(&mut serializer).unwrap();
        let writer = serializer.into_inner();
        let bytes_written = writer.bytes_written();
        assert_eq!(writer.into_inner()[..bytes_written], reference_bytes);
        for s in 0..10 {
            let mut rng = ChaCha20Rng::seed_from_u64(s);
            let base_name = "Some/";
            let base_time: f64 = rng.gen();
            let base_unit = "Unit";
            let base_value: f64 = rng.gen();
            let base_sum: f64 = rng.gen();
            let base_version: u64 = rng.gen();
            let name = "name";
            let unit = "A";
            let value: f64 = rng.gen();
            let sum: f64 = rng.gen();
            let time: f64 = rng.gen();
            let update_time: f64 = rng.gen();

            let record = Record::<Cbor> {
                base_name: Some(base_name),
                base_time: Some(base_time),
                base_unit: Some(base_unit),
                base_value: Some(base_value),
                base_sum: Some(base_sum),
                base_version: Some(base_version),
                name: Some(name),
                unit: Some(unit),
                value: Some(Values::Value(value)),
                sum: Some(sum),
                time: Some(time),
                update_time: Some(update_time),
                ..Default::default()
            };
            let mut buf = [0_u8; 1024];
            let writer = SliceWrite::new(&mut buf);
            let mut serializer = serde_cbor_serializer::new(writer);
            record.serialize(&mut serializer).unwrap();
            assert_ne!(serializer.into_inner().bytes_written(), 0);

            let record = Record::<Cbor> {
                base_name: None,
                base_time: None,
                base_unit: None,
                base_value: None,
                base_sum: None,
                base_version: None,
                name: Some(name),
                unit: Some(unit),
                value: Some(Values::Value(value)),
                sum: Some(sum),
                time: Some(time),
                update_time: Some(update_time),
                ..Default::default()
            };
            let mut buf = [0_u8; 1024];
            let writer = SliceWrite::new(&mut buf);
            let mut serializer = serde_cbor_serializer::new(writer);
            record.serialize(&mut serializer).unwrap();
            assert_ne!(serializer.into_inner().bytes_written(), 0);
        }
    }

    /// Deserializing a random [Record]
    #[test]
    fn test_deserialize_record() {
        let mut rng = ChaCha20Rng::from_rng(thread_rng()).unwrap();
        let mut record: Record<Cbor> = Record::new();
        record = Record {
            base_version: Some(rng.gen()),
            base_name: Some(&"Fraunhofer"),
            base_time: Some(rng.gen()),
            base_unit: Some(&"V"),
            base_value: Some(rng.gen()),
            base_sum: Some(rng.gen()),
            name: Some(&"-IML"),
            unit: None,
            value: Some(Values::Value(rng.gen())),
            sum: Some(rng.gen()),
            time: Some(rng.gen()),
            update_time: Some(rng.gen()),
            ..record
        };
        let mut buf = [0_u8; 1024];
        let serialized_record = {
            let writer = SliceWrite::new(&mut buf);
            let mut serializer = serde_cbor_serializer::new(writer);
            record.serialize(&mut serializer).unwrap();
            let bytes_written = serializer.into_inner().bytes_written();
            &buf[..bytes_written]
        };
        let mut scratch = [0_u8; 2048];

        let deser_record: Record<Cbor> =
            serde_cbor::de::from_slice_with_scratch(serialized_record, &mut scratch).unwrap();
        assert!(deser_record == record);
    }
}
