// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Everything regarding the actual Content of a SenML Message
//!
//! A Pack ([pack]) filled with Record ([record]) items can be serialized and deserialized.
//! A resolved Record ([resolved_record]) is a Record, that combines base fields with normal fields. That means that each resolved_record is a valid Pack.

pub(crate) mod label;
pub mod pack;
pub mod record;
pub mod resolved_record;
