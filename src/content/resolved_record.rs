// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! The [ResolvedRecord] combines base and normal fields.
//! For example the name field contains the concatenation of name and base_name.
//! [ResolvedRecord] is only accessed through the [RecordIterator] in [Pack]
//!
//! The specification S selects the format of serialization. Currently supported are:\
//!     1.Cbor
//!
//! In contrast to the SenMl specifications [ResolvedRecord] contains relative times
//! and [RecordIterator] doesn't sort the records by time.

use heapless::{String, Vec};

use crate::Cbor;

use super::{
    pack::Pack,
    record::{Record, Values},
};

/// Holds the information of one SenML record
#[derive(Debug, Clone)]
pub struct ResolvedRecord<'s, S: Clone> {
    /// Base version field
    pub base_version: Option<u64>,
    /// Base name field
    last_base_name: Option<&'s str>,
    /// Base time field
    last_base_time: Option<f64>,
    /// Base unit field
    last_base_unit: Option<&'s str>,
    /// Base value field
    last_base_value: Option<f64>,
    /// Base sum field
    last_base_sum: Option<f64>,
    /// Name field
    name: Option<&'s str>,
    /// Unit field
    unit: Option<&'s str>,
    /// Holds the value of the [Record], can be one of:\
    /// 1. value(f64)
    /// 2. StringValue(&'s str)
    /// 3. BooleanValue(bool)
    /// 4. DataValue(&'s [u8])
    value: Option<Values<'s>>,
    /// Sum field
    sum: Option<f64>,
    /// Time field
    time: Option<f64>,
    /// Update time field
    pub update_time: Option<f64>,

    _marker: core::marker::PhantomData<S>,
}

impl<'s, S: Clone> TryFrom<&Record<'s, S>> for ResolvedRecord<'s, S> {
    type Error = crate::Error;

    fn try_from(record: &Record<'s, S>) -> Result<Self, Self::Error> {
        let resolved_record = ResolvedRecord {
            base_version: record.base_version,
            last_base_name: record.base_name,
            last_base_time: record.base_time,
            last_base_unit: record.base_unit,
            last_base_value: record.base_value,
            last_base_sum: record.base_sum,
            name: record.name,
            unit: record.unit,
            value: record.value,
            sum: record.sum,
            time: record.time,
            update_time: record.update_time,
            _marker: record._marker,
        };
        resolved_record.validate()?;
        Ok(resolved_record)
    }
}

impl<'s, S: Clone> ResolvedRecord<'s, S> {
    /// returns the resolved name of the record\
    /// (base_name + name)
    pub fn resolved_name<const MAX_STRING_LENGTH: usize>(
        &self,
    ) -> Result<String<MAX_STRING_LENGTH>, crate::Error> {
        let mut resolved_name: String<MAX_STRING_LENGTH> = String::new();

        if let Some(base_name) = self.last_base_name {
            if resolved_name.push_str(base_name).is_err() {
                return Err(crate::Error::StringTooLong);
            }
        }
        if let Some(name) = self.name {
            if resolved_name.push_str(name).is_err() {
                return Err(crate::Error::StringTooLong);
            }
        }
        Ok(resolved_name)
    }

    /// returns the resolved unit of the record
    pub fn resolved_unit(&self) -> Option<&'s str> {
        if self.unit.is_some() {
            self.unit
        } else {
            self.last_base_unit
        }
    }

    /// returns the relative time to "now" of the record\
    /// returns None, if the resolved time value is greater than 2^28
    pub fn relative_time(&self) -> Option<f64> {
        let mut base_time = 0.0;
        let mut time = 0.0;

        if let Some(last_base_time) = self.last_base_time {
            base_time = last_base_time;
        }

        if let Some(record_time) = self.time {
            time = record_time;
        }
        time += base_time;
        if time < ((2_u64.pow(28)) as f64) {
            Some(time)
        } else {
            None
        }
    }

    /// returns the absolute time of the record\
    /// returns None, tif the resolved time is smaller than 2^28
    pub fn absolute_time(&self) -> Option<f64> {
        let mut base_time = 0.0;
        let mut time = 0.0;

        if let Some(last_base_time) = self.last_base_time {
            base_time = last_base_time;
        }

        if let Some(record_time) = self.time {
            time = record_time;
        }
        time += base_time;
        if time >= ((2_u64.pow(28)) as f64) {
            Some(time)
        } else {
            None
        }
    }

    /// returns the resolved sum of the record
    pub fn resolved_sum(&self) -> Option<f64> {
        match (self.last_base_sum, self.sum) {
            (Some(base_sum), Some(sum)) => Some(base_sum + sum),
            (Some(base_sum), None) => Some(base_sum),
            (None, Some(sum)) => Some(sum),
            (None, None) => None,
        }
    }

    /// returns the resolved_value of the record\
    /// returns None, if no f64 value is present
    pub fn resolved_value(&self) -> Option<f64> {
        match (self.last_base_value, self.value) {
            (Some(base_value), Some(Values::Value(value))) => Some(base_value + value),
            (None, Some(Values::Value(value))) => Some(value),
            (_, _) => None,
        }
    }

    /// returns the string_value of the record\
    /// returns None, if no string_value is present
    pub fn string_value(&self) -> Option<&'s str> {
        if let Some(Values::StringValue(value)) = self.value {
            Some(value)
        } else {
            None
        }
    }

    /// returns the boolean_value of the record\
    /// returns None, if no boolean_value is present
    pub fn boolean_value(&self) -> Option<bool> {
        if let Some(Values::BooleanValue(value)) = self.value {
            Some(value)
        } else {
            None
        }
    }
    /// returns the data_value of the record\
    /// returns None if no data_value is present
    pub fn data_value(&self) -> Option<&'s [u8]> {
        if let Some(Values::DataValue(value)) = self.value {
            Some(value)
        } else {
            None
        }
    }

    /// checks whether the [ResolvedRecord] is valid or not
    pub fn validate(&self) -> Result<(), crate::Error> {
        // Name
        if self.last_base_name.is_none() && self.name.is_none() {
            return Err(crate::Error::MissingName);
        }
        // Value
        if self.value.is_none() && self.sum.is_none() {
            return Err(crate::Error::MissingValueField);
        }
        Ok(())
    }
}

impl<'s, S: Clone> PartialEq for ResolvedRecord<'s, S> {
    fn eq(&self, other: &Self) -> bool {
        self.base_version == other.base_version
            && self.name == other.name
            && self.last_base_name == other.last_base_name
            && self.resolved_unit() == other.resolved_unit()
            && self.absolute_time() == other.absolute_time()
            && self.relative_time() == other.relative_time()
            && self.resolved_value() == other.resolved_value()
            && self.string_value() == other.string_value()
            && self.boolean_value() == other.boolean_value()
            && self.data_value() == other.data_value()
    }
}

/// Iterator that Iterates over a Vec of [Record]s and parses base fields
pub struct RecordIterator<
    's,
    S: Clone,
    const MAX_PACKAGE_LENGTH: usize = { crate::DEFAULT_MAX_PACKAGE_LENGTH },
> {
    entries: Vec<Record<'s, S>, MAX_PACKAGE_LENGTH>,
    pub(crate) next_index: usize,
    last_base_version: u64,
    last_base_name: Option<&'s str>,
    last_base_time: Option<f64>,
    last_base_unit: Option<&'s str>,
    last_base_value: Option<f64>,
    last_base_sum: Option<f64>,
    already_errored: bool,
    /// phantom data
    pub _marker: core::marker::PhantomData<S>,
}

impl<'s, S: Clone, const MAX_PACKAGE_LENGTH: usize> RecordIterator<'s, S, MAX_PACKAGE_LENGTH> {
    /// Creates a new [RecordIterator] on a given [Pack]
    pub fn new(package: &'s Pack<'s, S, MAX_PACKAGE_LENGTH>) -> Self {
        Self {
            entries: package.records(),
            next_index: 0,
            last_base_version: 10,
            last_base_name: None,
            last_base_time: None,
            last_base_unit: None,
            last_base_value: None,
            last_base_sum: None,
            already_errored: false,
            _marker: core::marker::PhantomData::default(),
        }
    }
}

impl<'s, const MAX_PACKAGE_LENGTH: usize> Iterator
    for RecordIterator<'s, Cbor, MAX_PACKAGE_LENGTH>
{
    type Item = Result<ResolvedRecord<'s, Cbor>, crate::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let index = self.next_index;
        self.next_index += 1;

        if self.already_errored {
            return None;
        }

        if let Some(record_iter) = self.entries.get(index) {
            let mut record = record_iter.clone();

            // Name
            if let Some(base_name) = record.base_name {
                self.last_base_name = Some(base_name);
            } else {
                record.base_name = self.last_base_name;
            }

            // Time
            if let Some(base_time) = record.base_time {
                self.last_base_time = Some(base_time);
            } else {
                record.base_time = self.last_base_time;
            }

            // Unit
            if let Some(base_unit) = record.base_unit {
                self.last_base_unit = Some(base_unit);
            } else {
                record.base_unit = self.last_base_unit;
            }

            // Value
            if let Some(base_value) = record.base_value {
                self.last_base_value = Some(base_value);
            }

            // Sum
            if let Some(base_sum) = record.base_sum {
                self.last_base_sum = Some(base_sum);
            } else {
                record.base_sum = self.last_base_sum;
            }

            // Base Version
            if let Some(base_version) = record.base_version {
                self.last_base_version = base_version;
            } else {
                record.base_version = Some(self.last_base_version);
            }

            Some(ResolvedRecord::try_from(&record))
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rand::prelude::*;
    use rand_chacha::ChaCha20Rng;

    /// testing resolving as random record
    #[test]
    fn test_resolving_record() {
        for s in 1..10 {
            let mut rng = ChaCha20Rng::seed_from_u64(s);
            let mut full_name: String<12> = String::new();
            full_name.push_str("Some/name").unwrap();
            let base_name = "Some/";
            let base_time: f64 = rng.gen();
            let base_unit = "Unit";
            let base_value: f64 = rng.gen();
            let base_sum: f64 = rng.gen();
            let base_version: u64 = rng.gen();
            let name = "name";
            let unit = "A";
            let value: f64 = rng.gen();
            let sum: f64 = rng.gen();
            let time: f64 = rng.gen();
            let update_time: f64 = rng.gen();

            let record = Record::<Cbor> {
                base_name: Some(base_name),
                base_time: Some(base_time),
                base_unit: Some(base_unit),
                base_value: Some(base_value),
                base_sum: Some(base_sum),
                base_version: Some(base_version),
                name: Some(name),
                unit: Some(unit),
                value: Some(Values::Value(value)),
                sum: Some(sum),
                time: Some(time),
                update_time: Some(update_time),
                ..Default::default()
            };
            let resolved_record = ResolvedRecord::try_from(&record).unwrap();

            assert_eq!(resolved_record.resolved_name::<12>().unwrap(), full_name);
            assert_eq!(resolved_record.resolved_unit().unwrap(), unit);
            if (time + base_time) < ((2 ^ 28) as f64) {
                assert_eq!(resolved_record.relative_time().unwrap(), time + base_time);
                assert_eq!(resolved_record.absolute_time(), None);
            } else {
                assert_eq!(resolved_record.absolute_time().unwrap(), time + base_time);
                assert_eq!(resolved_record.relative_time(), None);
            }
            assert_eq!(resolved_record.resolved_sum().unwrap(), sum + base_sum);
            assert_eq!(
                resolved_record.resolved_value().unwrap(),
                base_value + value
            );

            let record = Record::<Cbor> {
                base_name: None,
                base_time: None,
                base_unit: None,
                base_value: None,
                base_sum: None,
                base_version: Some(base_version),
                name: Some(name),
                unit: Some(unit),
                value: Some(Values::Value(value)),
                sum: Some(sum),
                time: Some(time),
                update_time: Some(update_time),
                ..Default::default()
            };
            let resolved_record = ResolvedRecord::try_from(&record).unwrap();
            if (time) < ((2 ^ 28) as f64) {
                assert_eq!(resolved_record.relative_time().unwrap(), time);
                assert_eq!(resolved_record.absolute_time(), None);
            } else {
                assert_eq!(resolved_record.absolute_time().unwrap(), time);
                assert_eq!(resolved_record.relative_time(), None);
            }
            assert_eq!(resolved_record.resolved_sum().unwrap(), sum);
            assert_eq!(resolved_record.resolved_value().unwrap(), value);
        }
    }
}
