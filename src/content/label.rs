// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Labels as defined by SenML

#[derive(Debug, Eq, PartialEq, Clone)]
pub(crate) enum Label {
    //Base Fields
    BaseVersion,
    BaseName,
    BaseTime,
    BaseUnit,
    BaseValue,
    BaseSum,
    //Normal Fields
    Name,
    Unit,
    Value,
    StringValue,
    BooleanValue,
    Sum,
    Time,
    UpdateTime,
    DataValue,
}

impl Label {
    pub(crate) fn cbor_value(&self) -> i8 {
        use Label::*;
        match self {
            BaseVersion => -1,
            BaseName => -2,
            BaseTime => -3,
            BaseUnit => -4,
            BaseValue => -5,
            BaseSum => -6,
            Name => 0,
            Unit => 1,
            Value => 2,
            StringValue => 3,
            BooleanValue => 4,
            Sum => 5,
            Time => 6,
            UpdateTime => 7,
            DataValue => 8,
        }
    }
}

impl TryFrom<i8> for Label {
    type Error = crate::Error;

    fn try_from(value: i8) -> Result<Self, crate::Error> {
        use Label::*;
        match value {
            -1 => Ok(BaseVersion),
            -2 => Ok(BaseName),
            -3 => Ok(BaseTime),
            -4 => Ok(BaseUnit),
            -5 => Ok(BaseValue),
            -6 => Ok(BaseSum),
            0 => Ok(Name),
            1 => Ok(Unit),
            2 => Ok(Value),
            3 => Ok(StringValue),
            4 => Ok(BooleanValue),
            5 => Ok(Sum),
            6 => Ok(Time),
            7 => Ok(UpdateTime),
            8 => Ok(DataValue),
            v => Err(crate::Error::UndefinedField(v)),
        }
    }
}
