// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! The [Pack] serves as a container for [Record] Items.
//!
//! The [Pack] is the part of the public interface that is actually
//! encodable and decodable. It can hold any [Record] items and supplies
//! convenience functions to add commonly used Record types.
//!
//! The specification S selects the format of serialization. Currently supported are:\
//!     1.Cbor

use heapless::Vec;
use serde::{de::Visitor, Deserialize, Serialize};

use crate::Cbor;

use super::{
    record::{Record, Values},
    resolved_record::RecordIterator,
};

/// Container for the records
#[derive(Clone, Debug)]
pub struct Pack<
    's,
    S: Clone,
    const MAX_PACKAGE_LENGTH: usize = { crate::DEFAULT_MAX_PACKAGE_LENGTH },
> {
    records: Vec<Record<'s, S>, MAX_PACKAGE_LENGTH>,
}

impl<'s, S: Clone, const MAX_PACKAGE_LENGTH: usize> Pack<'s, S, MAX_PACKAGE_LENGTH> {
    /// Create a new empty [Pack]
    pub fn new() -> Self {
        Pack {
            records: Vec::new(),
        }
    }

    /// Create a new [Pack] from given records
    pub fn from_vec(records: Vec<Record<'s, S>, MAX_PACKAGE_LENGTH>) -> Self {
        Pack { records }
    }

    pub(crate) fn records(&self) -> Vec<Record<'s, S>, MAX_PACKAGE_LENGTH> {
        self.records.clone()
    }

    /// Returns the amount of [Records](crate::content::record::Record) in the Pack
    pub fn len(&self) -> usize {
        self.records.len()
    }

    /// Returns of the Pack is empty or not
    pub fn is_empty(&self) -> bool {
        self.records.is_empty()
    }

    /// Returns the maximum amount of [Records](crate::content::record::Record) in the Pack
    pub fn capacity(&self) -> usize {
        self.records.capacity()
    }

    /// Returns an Iterator over the records
    pub fn records_iter(&'s self) -> RecordIterator<'s, S, MAX_PACKAGE_LENGTH> {
        RecordIterator::new(self)
    }

    /// Adds an Record to the [Pack]\
    /// Returns an error if [Pack] is already full
    pub fn add_record(&mut self, record: Record<'s, S>) -> Result<(), crate::Error> {
        self.records
            .push(record)
            .map_err(|_| crate::Error::TooManyEntries)?;
        Ok(())
    }

    /// Adds an Record containing a name and a value to the [Pack]\
    /// Returns an error if [Pack] is already full
    pub fn add_value_record(&mut self, name: &'s str, value: f64) -> Result<(), crate::Error> {
        let record = Record::<'s, S>::new();
        let record = Record {
            name: Some(name),
            value: Some(Values::Value(value)),
            ..record
        };
        self.add_record(record)
    }

    /// Adds an Record with a name and a string value to the [Pack]\
    /// Returns an error if [Pack] is already full
    pub fn add_string_value_record(
        &mut self,
        name: &'s str,
        value: &'s str,
    ) -> Result<(), crate::Error> {
        let record = Record::<'s, S>::new();
        let record = Record {
            name: Some(name),
            value: Some(Values::StringValue(value)),
            ..record
        };
        self.add_record(record)
    }

    /// Adds an Record containing a name and bool value to the [Pack]\
    /// Returns an error if [Pack] is already full
    pub fn add_boolean_record(&mut self, name: &'s str, value: bool) -> Result<(), crate::Error> {
        let record = Record::<'s, S>::new();
        let record = Record {
            name: Some(name),
            value: Some(Values::BooleanValue(value)),
            ..record
        };
        self.add_record(record)
    }

    /// Adds an [Record] containing a name, value and time to the [[Pack]]\
    /// Returns an error if [[Pack]] is already full
    pub fn add_time_value_record(
        &mut self,
        name: &'s str,
        time: f64,
        value: f64,
    ) -> Result<(), crate::Error> {
        let record = Record::<'s, S>::new();
        let record = Record {
            name: Some(name),
            time: Some(time),
            value: Some(Values::Value(value)),
            ..record
        };
        self.add_record(record)
    }
}

impl<'txt, S: Clone> Default for Pack<'txt, S> {
    fn default() -> Self {
        Self::new()
    }
}

impl<'txt, const MAX_PACKAGE_LENGTH: usize> Serialize for Pack<'txt, Cbor, MAX_PACKAGE_LENGTH> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.records.serialize(serializer)
    }
}

impl<'de, const MAX_PACKAGE_LENGTH: usize> Deserialize<'de>
    for Pack<'de, Cbor, MAX_PACKAGE_LENGTH>
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct PackageVisitor<
            const MAX_PACKAGE_LENGTH: usize = { crate::DEFAULT_MAX_PACKAGE_LENGTH },
        >;

        impl<'de, const MAX_PACKAGE_LENGTH: usize> Visitor<'de> for PackageVisitor<MAX_PACKAGE_LENGTH> {
            type Value = Pack<'de, Cbor, MAX_PACKAGE_LENGTH>;

            fn expecting(&self, formatter: &mut core::fmt::Formatter) -> core::fmt::Result {
                write!(formatter, "expecting a Sequence of CborEncodableEntries")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: serde::de::SeqAccess<'de>,
            {
                let mut entries: Vec<Record<'de, Cbor>, MAX_PACKAGE_LENGTH> = Vec::new();
                while let Some(entry) = seq.next_element::<Record<'de, Cbor>>()? {
                    entries.push(entry).unwrap();
                }
                Ok(Pack::<'de, Cbor, MAX_PACKAGE_LENGTH>::from_vec(entries))
            }
        }
        deserializer.deserialize_seq(PackageVisitor)
    }
}
